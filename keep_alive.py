
import time
import psutil as ps
from lib.mqtt import MqttObject
from lib.conf import ConfigurationObject
from lib.gps import GPSObject
from lib.elements import Core


configuration = ConfigurationObject()
ID_SINAPSIS = configuration.config_dict['sinapsis']['id']
time_laps = 30


def build_json_status(data_to_build_json):
    disk_vars = ps.disk_usage('/')
    ram_vars = ps.virtual_memory()
    const_mb = float(1024 ** 2)
    const_gb = float(1024 ** 3)

    temp_int = os.popen("vcgencmd measure_temp").readline()
    temp_int = temp_int.replace("temp=", "")
    temp_int = float(temp_int[0:temp_int.find(".")])
    temp_int = round(temp_int,2)


    json_status = {
        "id": ID_SINAPSIS,
        "variables": {
            "cpu_usage": "{}%".format(float(ps.cpu_percent())),
            "disk_total":  "{0:.2f}GB".format(float(disk_vars.total)/const_gb),
            "disk_usage": "{0:.2f}GB".format(float(disk_vars.used)/const_gb),
            "ram_total": "{0:.0f}MB".format(float(ram_vars.total)/const_mb),
            "ram_usage": "{0:.0f}MB".format(float(ram_vars.used)/const_mb),
            "temperature": temp_int,
            "sw": data_to_build_json['sw'],
            "gps":data_to_build_json['gps']
        }
    }

    #if temperature >80: APAGA
    return json_status



def keep_alive():
    mqtt_client = MqttObject()
    mqtt_client.run()
    gps_client = GPSObject()
    core = Core()

    while True:
        while mqtt_client.connected:
            data_to_build_json = {}

            gps_mode = gps_client.get_mode()
            data_to_build_json['gps'] = True if gps_mode >= 2 else False

            try:
                sw = core.mcu_query('rpi_mode')[1]
            except Exception as e:
                sw = str(e)
            finally:
                if sw==1:
                    sw=True
                elif sw==2:
                    sw=False

            data_to_build_json['sw'] = sw

            json_status = build_json_status(data_to_build_json)

            mqtt_client.publish_message(msg=json_status,
                                        topic=configuration.config_dict['mqtt']['topic_status'])
            time.sleep(time_laps)

        while not mqtt_client.connected:
            print("MQTT disconnected, the keep_alive JSON can't be sended")
            mqtt_client.run()
            time.sleep(time_laps)



if __name__ == "__main__":
    
    try:
        keep_alive()
    except KeyboardInterrupt:
        print('Keep_Alive Main Script stopped\n')
