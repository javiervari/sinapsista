import os
import datetime, time
import gpsd
import geopy.distance

from .rtctime import datetime_now


gpsd.connect()

class GPSObject():
    """ 
    This is a class to handles the GPS.

    Attributes:
        data (dict): The object with gps's data
    """
    def __init__(self):
        """ 
        The constructor for GPSclient class.   
        """
        print("Inicializando GPS...")
        gps =  768
        while gps == 768:
            gps = os.system('sudo systemctl is-active --quiet gpsd')
            time.sleep(1)
            if gps == 768:
                print("Activating GPS service")
                gps = os.system('sudo systemctl start gpsd')
        time.sleep(3)

        self.last_coord = None
        self.perimeter_radio = 5
        self.traveled_distance = 0




    def store_first_position(self):
        self.initial_datetime = datetime_now() #Date and Time from RTC
        gps_data = self.get_data()
        while gps_data==False or gps_data['mode']<=1:
            gps_data = self.get_data()
            time.sleep(0.5)
            print('Searching GPS initial info...')

        self.start_coordinates = "{};{}".format(gps_data['latitude'],
                                                gps_data['longitude'])
        

    def set_last_coord(self, last_coord):
        """Las coordenadas recibidas por parametro seran establecidas 
        como las ultimas coordenadas que envio el Sinapsis
        
        Arguments:
            last_coord tuple(float(),float()) -- Coordenadas a ser enviadas por MQTT
        """
        self.last_coord = last_coord




    def distance_between_two_point(self, p1, p2, unit):
        if unit == 'm':
            d = round(geopy.distance.vincenty(p1,p2).m)
            return d
        if unit == 'km':
            d = (geopy.distance.vincenty(p1,p2).m)/1000
            return d


    def perimeter(self, current_coord):
        if self.last_coord is None: #FIRST LOOP, no matter it return True, is the first loop
            return True
        else:
            distance_between_theirs = self.distance_between_two_point(self.last_coord, current_coord, 'm')

            print("COORDENADA ACTUAL", current_coord)
            print("COORDENADA ANTERIOR", self.last_coord)
            print("DISTANCIA ENTRE ELLOS",distance_between_theirs,"m")
            if distance_between_theirs >= self.perimeter_radio:
                print('LA UNIDAD SE ESTA MOVIENDO PORQUE LA COORDENADA ANTERIOR ES {} Y LA ACTUAL ES {}, POR LO TANTO HAY {}m DE DISTANCIA ENTRE ELLOS'\
                    .format(self.last_coord, current_coord, distance_between_theirs)
                    )
                return True
            else:
                return False





    def total_traveled_distance(self, current_coord):
        distance = self.distance_between_two_point(self.last_coord, current_coord, 'km')
        self.traveled_distance += distance
        return round(self.traveled_distance,2)




    def get_mode(self):
        position = gpsd.get_current()
        return position.mode


    def get_data(self):
        """ 
        The function to get the GPS's data. 
  
        Returns: 
            data: A dict with data
        """

        self.position = gpsd.get_current()
        gps_mode = self.position.mode
        # print("GPS MODE: {}".format(gps_mode))
        if gps_mode >= 2:

            latitude = self.position.position()[0]
            longitude = self.position.position()[1]
            speed = self.position.hspeed #in m/s

            if gps_mode==3:
                altitude = self.position.altitude() #3D Fix
                motion_dir = self.position.movement()['track'] #3D Fix
                climb = self.position.movement()['climb'] #3D Fix
            else:
                altitude = 0
                motion_dir = 0
                climb = 0

            self.data = {
                'mode': gps_mode,
                'latitude': latitude,
                'longitude': longitude,
                'speed': int(3.6*speed), #in km/h
                'altitude': altitude,
                'motion_dir': motion_dir,
                'climb': climb,
                'date_time': datetime_now() #datetime obj from rtc in UTC
            }

        else:
            self.data = False

        return self.data





    def info_travel(self):
        time.sleep(0.5) #to wait... maybe it is not necessary

        if self.last_coord is None: #FIRST LOOP
            print("First loop ")
            info_travel_dict = {"start_datetime": str(self.initial_datetime),
                    "start_coord": self.start_coordinates,
                    "traveled_distance": 0,
                    "accumulated_time": str(datetime_now() - self.initial_datetime),
                    "avg_speed": 0 #por desarrollar
                    }
            
            return info_travel_dict
        else:
            info_travel_dict = {"start_datetime": str(self.initial_datetime),
                    "start_coord": self.start_coordinates,
                    "traveled_distance": self.total_traveled_distance(current_coord=current_coord),
                    "accumulated_time": str(datetime_now() - self.initial_datetime),
                    "avg_speed": 0 #por desarrollar
                    }
            return info_travel_dict












# if __name__ == "__main__":
#     gps = GPSclient()
#     while 1:
#         R = gps.get_data()
#         print(R)
#         time.sleep(2)
    
