import datetime, time
import json


def storage_message(msg, now):
    current_filename = "/home/pi/sinapsisT/offlinedata/data_{}.json".format(now.strftime('%Y_%m_%d_%H'))
    try:
        json_data = json.load(open(current_filename))
    except FileNotFoundError:
        json_data = []

    json_data.append(msg)
    with open(current_filename, 'w') as json_file:
        json.dump(json_data,json_file)

    return