import datetime, time, pytz
import subprocess


def datetime_now():
    datetime_string = None
    while datetime_string == None:
        try:
            time.sleep(0.1)
            datetime_string = subprocess.check_output('sudo hwclock -r'.split()).decode('utf-8').strip('\n')
        except subprocess.CalledProcessError:
            time.sleep(0.1)
            pass
    
    datetime_obj = datetime.datetime.strptime(datetime_string, '%Y-%m-%d %H:%M:%S.%f%z')
    return datetime_obj.astimezone(pytz.UTC) #return it in UTC

