
import os, time, subprocess, sys
import ast
import smbus2
import binascii
import crcmod
import math
import threading
import RPi.GPIO as GPIO

from .simpleble import *
from .conf import ConfigurationObject
from FaBo9Axis_MPU9250 import FaBo9Axis_MPU9250


class MCUObject(ConfigurationObject):
    def __init__(self):
        ConfigurationObject.__init__(self)

        self.bus = smbus2.SMBus(1)
        self.address = 0x01
        self.init = 0xAA
        self.end = 0xEE
        self.ack = 0xFF
        self.nack = 0xF0


    def crc(self, string):
        crc8_func = crcmod.predefined.mkPredefinedCrcFun('crc-8')
        crc = (hex(crc8_func(bytearray((string)))))
        return crc


    def send_command(self, command):
        data = [0xAA, command, 0xCC, 0xEE]
        crc8 = self.crc(data[1:2])
        data[2]=int(crc8,0)
        self.bus.write_i2c_block_data(self.address,0,data)
        time.sleep(1)


    def answer(self):
        block = (self.bus.read_i2c_block_data(self.address,0,4))
        #print("block[2]", hex(block[2]), "getcrc", getcrc)
        #Check data received
        if(block[0]==self.init and block[3]==self.end):
            getcrc = self.crc(block[1:2])
            if(block[1]==self.nack): print("Error in sent data")
            if(hex(block[2])==getcrc): print("Correct data")
        return block


    def mcu_query(self, command):
        if command == 'rpi_mode': #sleep(0x02) o normal(0x01)
            command = 0x00

        elif command == 'mcu_events': #Panic(0x11), shutdown(0x12), restart(0x13), request_stop(0x14), change_mode(0x15)
            command = 0x10

        elif command == 'sensors': #temperatura(0x20)
            command = 0x20

        elif command == 'mcu_program': #programar el mcu
            command = 0x30


        self.send_command(command)
        answer = self.answer()
        return answer



    

class RPIObject(ConfigurationObject):
    def __init__(self):
        ConfigurationObject.__init__(self)

        self.gpio_life_signal = ast.literal_eval(self.config_dict['MCU']['gpio_life_signal'])
        self.gpio_request_i2c = ast.literal_eval(self.config_dict['MCU']['gpio_request_i2c'])
        self.gpio_mcu_programmer = ast.literal_eval(self.config_dict['MCU']['gpio_mcu_programmer'])
        self.gpio_rpi_enable = ast.literal_eval(self.config_dict['MCU']['gpio_rpi_enable'])

        self.internet = 0 #limit 10

        
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        
        GPIO.setup(self.gpio_life_signal, GPIO.OUT)
        GPIO.setup(self.gpio_request_i2c, GPIO.IN)

        
        self.base_path = os.path.abspath('.')


    def internet_status_thread(self): #cambiar por clase de modulo gsm(class GSMModule)
        threading.Timer(5, self.internet_status_thread).start()
        hostname = 'google.com'
        response = os.system("ping -c 1 {} >/dev/null 2>&1".format(hostname))
        if response == 0:
            self.internet = 0
        else:
            self.internet +=1
            if self.internet > 5:
                print('Reinicio del modulo GSM...')
                os.system('sudo systemctl restart mobile.service &')


    def execute(self, file):
        if file == 'gadgets':
            file = self.base_path+'/gadgets.py'
        elif file == 'keep_alive':
            file = self.base_path+'/keep_alive.py'

        else:
            return False
        
        cmd_string = 'python3 {}'.format(file)
        process = subprocess.Popen(cmd_string.split(), stdout=sys.stdout, stderr=sys.stderr)
        return process



    def life_signal(self):
        while True:
            GPIO.output(self.gpio_life_signal, GPIO.HIGH)
            time.sleep(3)
            GPIO.output(self.gpio_life_signal, GPIO.LOW)
            time.sleep(3)











class Core(MCUObject, RPIObject):
    def __init__(self):
        MCUObject.__init__(self)
        RPIObject.__init__(self)


    def who_interrupted_the_rpi(self, channel):
        print('QUIEN INTERRUMPIO ??????????????????')
        cause = self.mcu_query('mcu_events')  #Panic(0x11), shutdown(0x12), restart(0x13), request_stop(0x14), change_mode(0x15)

        if cause==0x11:
            print('Es panico !!!!!!!!!!1')

        elif cause==0x12:
            print('HAY QUE APAGARLOOOOOOOOO')
        elif cause==0x13:
            print('HAY QUE REINICIARLOOOOOO')
        elif cause==0x14:
            print("SOLICITUD DE PARADAAAA")
        elif cause==0x15:
            print('CAMBIAR MODO DE REPORTEEEEE')



    def detect_rpi_interruption(self):
        GPIO.add_event_detect(self.gpio_request_i2c, 
                            GPIO.RISING, 
                            callback=self.who_interrupted_the_rpi, bouncetime=500)
        








class DoorSensor(ConfigurationObject):
    def __init__(self):
        ConfigurationObject.__init__(self)
        self.door_UUID = UUID(0x1600)
        self.battery_UUID = UUID(0x2A19)


        self.gpio_counter = ast.literal_eval(self.config_dict['MCU']['gpio_counter'])
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.gpio_counter, GPIO.OUT)
        GPIO.output(self.gpio_counter, GPIO.LOW)


            

    def run_door_sensor(self):
        self.bleClient = SimpleBleClient()
        while not self.bleClient.isConnected():
            try:
                device = self.bleClient.searchDevice(name="Open Door Sensor", timeout=1)
                if device is not None:
                    if self.bleClient.connect(device):
                        services = device.getServices()
                        door = self.bleClient.getCharacteristics(uuids=[self.door_UUID])[0]
                        battery = self.bleClient.getCharacteristics(uuids=[self.battery_UUID])[0]
                        if door and battery:
                            while True:
                                try:
                                    door_bytes = self.bleClient.readCharacteristic(door)
                                    door_str = "".join(map(chr, door_bytes))
                                    battery_bytes = self.bleClient.readCharacteristic(battery)
                                    battery_str = "".join(map(chr, battery_bytes))
                                    if door_str == '1':
                                        print ("Open Door")
                                        GPIO.output(self.gpio_counter, GPIO.HIGH)
                                    elif (door_str == '0'):
                                        print ("Closing Door")
                                        GPIO.output(self.gpio_counter, GPIO.LOW)
                                    time.sleep(1)

                                except BTLEException as e:
                                    GPIO.output(self.gpio_counter, GPIO.LOW)
                                    if(e != self.bleClient.isConnected()):
                                        self.bleClient.disconnect()
                                        break

                                except BrokenPipeError as e:
                                    GPIO.output(self.gpio_counter, GPIO.LOW)
                                    
                    else:
                        time.sleep(2)
                else:
                    time.sleep(2)


            except BTLEException as e:
                # If we get disconnected from the device, keep looping until we have reconnected
                if(e != self.bleClient.isConnected()):
                    self.bleClient.disconnect()
                    # print("Connection to BLE device has been lost!")
                    break

            except KeyboardInterrupt as e:
                # Detect keyboard interrupt and close down bleClient 
                self.bleClient.disconnect()
                raise e




class IMU():
    def __init__(self):
        self.mpu9250 = FaBo9Axis_MPU9250.MPU9250()

    def accelerometer(self):
        acc_raw = self.mpu9250.readAccel()
        self.ax=round((acc_raw['x'])*9.8,2)
        self.ay=round((acc_raw['y'])*9.8,2)
        self.az=round((acc_raw['z'])*9.8 - 3.7, 2)
        acceleration = {
            'x':self.ax,
            'y':self.ay,
            'z':self.az
        }
        return acceleration

        

    def gyroscope(self):
        gyro_raw = self.mpu9250.readGyro()
        gyroscope_data = {
            'x':round((gyro_raw['x'])*0.16,2),
            'y':round((gyro_raw['y'])*0.16,2),
            'z':round((gyro_raw['z'])*0.16,2)
        }
        return gyroscope_data

    def magnetometer(self):
        mag_raw = self.mpu9250.readMagnet()
        self.mx=round((mag_raw['x'])*0.01,2)
        self.my=round((mag_raw['y'])*0.01,2)
        self.mz=round((mag_raw['z'])*0.01,2)
        magnetometer_data = {
            'x':self.mx,
            'y':self.my,
            'z':self.mz
        }
        return magnetometer_data

    
    def angles(self):
        pitch = round(180*(math.atan2(self.ax,math.sqrt(self.ay*self.ay + self.az*self.az)))/math.pi,2)
        roll = round(180*(math.atan2(self.ay,math.sqrt(self.ax*self.ax + self.az*self.az)))/math.pi,2)

        yaw_x = self.mx*math.cos(pitch) + self.my*math.sin(roll)*math.sin(pitch) + self.mz*math.cos(roll)*math.sin(pitch)
        yaw_y = self.my*math.cos(roll) - self.mz*math.sin(roll)

        yaw =  round(180*(math.atan2(-yaw_y,yaw_x))/math.pi,2)

        return {
            'pitch': pitch,
            'roll':roll,
            'yaw':yaw

        }



    def temp(self):
        temperature = self.mpu9250.readTemperature()
        return temperature