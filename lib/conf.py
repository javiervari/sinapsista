import os

from configparser import ConfigParser

class ConfigurationObject(): 
    def __init__(self):
        self.conf_path = '/home/pi/sinapsisREFACTOR/conf.cfg'
        self.config=ConfigParser() 
        self.config.read(self.conf_path) 
        if self.config['sinapsis']['id'] == '': 
            self.serialization()
            self.config.set('sinapsis', 'id', self.sinapsis_id)
            with open(self.conf_path, 'w') as configfile:
                self.config.write(configfile)

        self.values()


    def serialization(self):
        # cmd_to_get_mac = os.popen("arp -a | grep '(192.168.1.1)' | awk '{ print $4 }'")
        # output = cmd_to_get_mac.read()
        # router_mac = output[:-1].split(':')
        MAC = open('/sys/class/net/eth0/address').read()[0:17].replace(':', '').upper()[-6::]
        self.sinapsis_id = 'VKA-T-A011-CA{}-{}'.format(MAC[0::2], MAC[2:6])


    def values(self):
        sections = self.config.sections()
        self.config_dict = {}

        for section in sections:
            self.config_dict[section] = {}

            for item in self.config.items(section):
                self.config_dict[section][item[0]] = item[1]



