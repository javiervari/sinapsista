import os
import time
import threading

from lib.rtctime import datetime_now
from lib.conf import ConfigurationObject
from lib.mqtt import MqttObject
from lib.gps import GPSObject
from lib.elements import Core, IMU


configuration = ConfigurationObject()
ID_SINAPSIS = configuration.config_dict['sinapsis']['id']

time_laps = 3


def build_json_data(data_to_build_json):
    json_data = {
            'sinapsis':{
                'id': ID_SINAPSIS,
                'mode': data_to_build_json['mode']
            },
            'devices':{
                'gps':{
                    'latitude': data_to_build_json['gps_data']['latitude'],
                    'longitude': data_to_build_json['gps_data']['longitude'],
                    'speed': data_to_build_json['gps_data']['speed'],
                    'altitude': data_to_build_json['gps_data']['altitude'],
                    'motion_direction': data_to_build_json['gps_data']['motion_dir'],
                    'climb': data_to_build_json['gps_data']['climb'] 
                },
                'imu':{
                    'accelerometer':{
                        'x':data_to_build_json['accelerometer']['x'],
                        'y':data_to_build_json['accelerometer']['y'],
                        'z':data_to_build_json['accelerometer']['z']
                    },
                    'magnetometer':{
                        'x':data_to_build_json['magnetometer']['x'],
                        'y':data_to_build_json['magnetometer']['y'],
                        'z':data_to_build_json['magnetometer']['z']
                    },
                    'gyroscope':{
                        'x':data_to_build_json['gyroscope']['x'],
                        'y':data_to_build_json['gyroscope']['y'],
                        'z':data_to_build_json['gyroscope']['z']
                    },
                    'angles':{
                        'roll':data_to_build_json['angles']['roll'],
                        'yaw':data_to_build_json['angles']['yaw'],
                        'pitch':data_to_build_json['angles']['pitch']
                    },
                    'temp':data_to_build_json['temp'],
                },
                'buttons':{
                    'panico': False #por definir
                }
            },
            'info_travel': data_to_build_json['info_travel'],
            'message_details': {
                'date': str(data_to_build_json['gps_data']['date_time']), 
                'id_message': '0'
                }
        }

    return json_data



def main():
    global keep_alive
    core = Core()
    core.internet_status_thread()
    
    core.detect_rpi_interruption()
    
    # core.execute('gadgets') #REVISAR ble, poner except e interrupcion de teclado o algo asi para cerrar conxion del 
    keep_alive = core.execute('keep_alive')
    
    threading.Thread(target=core.life_signal).start()
    

    gps_client = GPSObject()
    storage_first_position_thread = threading.Thread(target=gps_client.store_first_position)
    storage_first_position_thread.start()
    

    try:
        rpi_mode = core.mcu_query('rpi_mode')[1]
    except Exception as e:
        rpi_mode = str(e)
    finally:
        if rpi_mode==1:
            rpi_mode='normal'
        elif rpi_mode==2:
            rpi_mode='sleep'
    


    mqtt_client = MqttObject()
    mqtt_client.run()
    
    imu_client = IMU()
    

    while True:
        while mqtt_client.connected == True:
            gps_data = gps_client.get_data()
            
            if gps_data:
                print('Velocidad: {}Km/h'.format(gps_data['speed']))
                inside_perimeter = gps_client.perimeter(
                    (gps_data['latitude'],gps_data['longitude'])
                    )

                if inside_perimeter:
                    data_to_build_json = {}

                    info_travel = gps_client.info_travel()
                    data_to_build_json['info_travel'] = info_travel
                    data_to_build_json['mode'] = rpi_mode
                    data_to_build_json['gps_data'] = gps_data

                    data_to_build_json['accelerometer'] = imu_client.accelerometer()
                    data_to_build_json['gyroscope'] = imu_client.gyroscope()
                    data_to_build_json['magnetometer'] = imu_client.magnetometer()
                    data_to_build_json['angles'] = imu_client.angles()
                    data_to_build_json['temp'] = imu_client.temp()

                    data = build_json_data(data_to_build_json)
                    mqtt_client.publish_message(msg=data)

                    gps_client.set_last_coord(
                        last_coord=(gps_data['latitude'],gps_data['longitude']))

                    gps_data=False
            else:
                print('GPS not available')

            time.sleep(time_laps) #Tiempo de cada vuelta para el envio de coordenadas, ajustar a variable

        while not mqtt_client.connected:
            mqtt_client.run()
            gps_data = gps_client.get_data()

            if gps_data:
                inside_perimeter = gps_client.perimeter(
                    (gps_data['latitude'],gps_data['longitude'])
                    )

                if inside_perimeter:

                    print("MQTT disconnected, the messages will be stored in a JSON file...")
                    data_to_build_json = {}
                    info_travel = gps_client.info_travel()

                    data_to_build_json['info_travel'] = info_travel
                    data_to_build_json['mode'] = rpi_mode
                    data_to_build_json['gps_data'] = gps_data

                    data_to_build_json['accelerometer'] = imu_client.accelerometer()
                    data_to_build_json['gyroscope'] = imu_client.gyroscope()
                    data_to_build_json['magnetometer'] = imu_client.magnetometer()
                    data_to_build_json['angles'] = imu_client.angles()
                    data_to_build_json['temp'] = imu_client.temp()
                    

                    data = build_json_data(data_to_build_json)

                    storage_message(msg=data, now=gps_data['date_time'])

                    gps_client.set_last_coord(
                        last_coord=(gps_data['latitude'],gps_data['longitude']))

                    gps_data=False
                    
            time.sleep(time_laps)





if __name__ == '__main__':
    print('Initial Time :', datetime_now())
    try:
        main()
    except KeyboardInterrupt:
        print('Sinapsis Main Script stopped\n')
        keep_alive.terminate()
        print('Keep_Alive Main Script stopped\n')
